## BackwardModellingABR

This repository contains code and data used to generate the figures for Schmidt, F., Demarchi, G., Geyer, F., & Weisz, N. (2020). A backward encoding approach to recover subcortical auditory activity. NeuroImage, 116961.

New to Python, but still want to reproduce the figures?  
1. install anaconda as package manager  
2. open a console and cd in the directory you cloned this repository  
3. run "conda env create --file environment.yml" to install all necessary dependencies  
4. run "conda activate BackwardModellingABR" to activate the generated environment  
5. run "jupyter notebook" to open a notebook server and then just click on the notebooks you want to open :-)  